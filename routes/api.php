<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::get('allergens', 'AllergensController');

Route::get('activities', 'ActivitiesController');

Route::post('bmr', 'BmrController');

Route::get('nutrients', 'NutrientsController');

Route::get('meals', 'MealsController@index');

Route::get('meals/{id}', 'MealsController@show');

Route::middleware('auth:api')->group(function () {
    Route::get('weights', 'WeightController');
    Route::get('weights/info', 'WeightController@info');
    Route::get('trainings', 'WorkoutController@index');
    Route::post('trainings', 'WorkoutController@store');
    Route::get('trainings/calendar', 'Workout\CalendarController');
});



