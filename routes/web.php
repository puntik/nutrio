<?php

use App\Mail\WorkoutReport;
use Illuminate\Support\Facades\Route;
use Laravel\Socialite\Facades\Socialite;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController');
Route::get('/bmi', 'BmiController');
Route::get('/allergens', 'AllergensController');
Route::get('/activities', 'ActivitiesController');
Route::get('/nutrients', 'NutrientsController');
Route::get('/daily-menu', 'DailyMenuController');

Route::get('/auth/redirect', 'Auth\GoogleController@redirect');
Route::get('/auth/callback', 'Auth\GoogleController@callback');

Route::get('/mailable', function() {
    return new WorkoutReport();
});

Route::middleware('auth')->group(
    function () {
        Route::get('/weight', 'WeightController@index')->name('dashboard');
        Route::post('/weight', 'WeightController@store');

        Route::get('/workout', 'WorkoutController@index');

        Route::controller('ProfileController')->group(function () {
            Route::get('/profile', 'edit');
            Route::post('/profile', 'store');
        });

        Route::get('/chartjs', function () {
            return view('weight.chartjs');
        });

        Route::get('/react', function() {
            return view('react');
        });
    }
);
