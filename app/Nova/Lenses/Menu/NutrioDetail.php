<?php

namespace App\Nova\Lenses\Menu;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\LensRequest;
use Laravel\Nova\Lenses\Lens;

class NutrioDetail extends Lens
{

    /**
     * Get the query builder / paginator for the lens.
     *
     * @param \Laravel\Nova\Http\Requests\LensRequest $request
     * @param \Illuminate\Database\Eloquent\Builder   $query
     *
     * @return mixed
     */
    public static function query(LensRequest $request, $query)
    {
        return $request->withOrdering($request->withFilters(
            $query
                ->select([
                    'menus.id',
                    'menus.title',
                    DB::raw('SUM(meals.energy * meal_menu.amount) AS energy'),
                    DB::raw('SUM(meals.proteins * meal_menu.amount) AS proteins'),
                    DB::raw('SUM(meals.carbohydrates * meal_menu.amount) AS carbohydrates'),
                    DB::raw('SUM(meals.sugars * meal_menu.amount) AS sugars'),
                    DB::raw('SUM(meals.fats * meal_menu.amount) AS fats'),
                ])
                ->join('meal_menu', 'menus.id', '=', 'meal_menu.menu_id')
                ->join('meals', 'meals.id', '=', 'meal_menu.meal_id')
                ->groupBy('menus.id')
        ));
    }

    /**
     * Get the fields available to the lens.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make('title'),
            Text::make('ener', 'energy'),
            Text::make('prot', 'proteins'),
            Text::make('carb', 'carbohydrates'),
            Text::make('suga', 'sugars'),
            Text::make('fats'),
        ];
    }

    /**
     * Get the cards available on the lens.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the lens.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available on the lens.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function actions(Request $request)
    {
        return parent::actions($request);
    }

    /**
     * Get the URI key for the lens.
     *
     * @return string
     */
    public function uriKey()
    {
        return 'menu-nutrio-detail';
    }
}
