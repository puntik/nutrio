<?php

namespace App\Nova\Actions;

use App\Model\Menu;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;
use Laravel\Nova\Fields\Text;

class AddMealIntoMenuAction extends Action
{

    use InteractsWithQueue, Queueable;

    /**
     * Perform the action on the given models.
     *
     * @param ActionFields $fields
     * @param Collection   $models
     *
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        $menuId = (int) $fields->get('menu_id');

        $menu = Menu::find($menuId);

        if ($menu === null) {
            return Action::danger(
                sprintf(
                    'Menu "#%d" not found.',
                    $menuId
                )
            );
        }

        $menu->meals()->syncWithPivotValues(
            $models,
            [
                'amount' => (int) $fields->get('amount'),
            ]
        );
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields()
    {
        return [
            Text::make('Menu id'),
            Text::make('Amount'),
        ];
    }
}
