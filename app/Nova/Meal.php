<?php

namespace App\Nova;

use App\Nova\Actions\AddMealIntoMenuAction;
use App\Nova\Metrics\NewMeals;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;

class Meal extends Resource
{

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Model\Meal::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'title',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param Request $request
     *
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Text::make('title')->sortable(),

            Text::make('energy')->sortable(),

            Text::make('proteins'),

            Text::make('carbohydrates'),
            Text::make('sugars')->hideFromIndex(),

            Text::make('fats'),
            Text::make('fatty_acid')->hideFromIndex(),

            Text::make('fibre')->hideFromIndex(),

            Text::make('salt')->hideFromIndex(),

            BelongsToMany::make('Allergens'),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param Request $request
     *
     * @return array
     */
    public function cards(Request $request)
    {
        return [
            new NewMeals(),
        ];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param Request $request
     *
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param Request $request
     *
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param Request $request
     *
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            new AddMealIntoMenuAction(),
        ];
    }
}
