<?php

namespace App\Mail;

use App\Model\WorkoutGroup;
use App\Services\StatsService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class WorkoutReport extends Mailable
{
    use Queueable, SerializesModels;

    public $data = [];

    /**
     * Create a new message instance.
     */
    public function __construct()
    {
        $this->data = array_fill(0,13,'2024-12-12');
    }

    /**
     * Get the message envelope.
     */
    public function envelope(): Envelope
    {
        return new Envelope(
            subject: 'Workout Report',
        );
    }

    /**
     * Get the message content definition.
     */
    public function content(): Content
    {
        $start = now()->subDays(14);
        $summary = (new StatsService)->statsForUser(1)->pluck('id', 'count');
        $workouts = (new StatsService)->statsByDay(1);
        $groups = WorkoutGroup::orderBy('id')->get()->pluck('title', 'id');
                
        return new Content(
            markdown: 'mail.workout-report',
            with: [
                "start" => $start,
                "workoutGroups" => $groups,
                'workouts'=>$workouts,
                "summary" => $summary
            ]
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array<int, \Illuminate\Mail\Mailables\Attachment>
     */
    public function attachments(): array
    {
        return [];
    }
}
