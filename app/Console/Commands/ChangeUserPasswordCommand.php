<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class ChangeUserPasswordCommand extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:password';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change user password';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->anticipate('Please, provide an email to be changed', function ($input): array {
            if (strlen($input) < 3) {
                return [];
            }

            return User::where('email', 'like', $input . '%')
                       ->pluck('email')
                       ->toArray();
        });

        try {
            $user           = User::whereEmail($email)->firstOrFail();
            $password       = $this->secret(sprintf('Type a new password for user "%s"', $email));
            $user->password = Hash::make($password);
            $user->save();
        } catch (\Exception $e) {
            $this->error('Cannot change the password. Is provided email correct?');

            return static::FAILURE;
        }

        $this->info(sprintf('Password for user "%s" was changed.', $email));

        return static::SUCCESS;
    }
}
