<?php

namespace App\Console\Commands\Strava;

use App\User;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Console\Command;

class RefreshToken extends Command
{
    protected $signature = 'strava:refresh-token
        {user_id : User to refresh token}
    ';

    protected $description = 'Refresh oauth token on strava connection';

    public function handle()
    {
        $userId = $this->argument('user_id');
        $user = User::findOrFail($userId);

        $accessToken = $this->refreshToken($user);

        $this->info(sprintf('New access token: %s', $accessToken));
    }

    private function refreshToken(User $user)
    {
        $strava_url = 'https://www.strava.com/api/v3/oauth/token';

        $client = new Client();

        $headers = [
            'Content-Type' => 'application/json'
        ];

        $body = [
            'client_id'     => config('services.strava.client_id'),
            'client_secret' => config('services.strava.client_secret'),
            'refresh_token' => $user->strava_refresh_token,
            'grant_type'    => 'refresh_token',
        ];

        $request = new Request('POST', $strava_url, $headers, json_encode($body));

        $res = $client->sendAsync($request)->wait();

        $oauth = json_decode($res->getBody(), true);

        $user->update([
            'strava_access_token' => $oauth['access_token'],
        ]);

        return $oauth['access_token'];
    }
}
