<?php

namespace App\Console\Commands\Strava;

use App\User;
use Illuminate\Console\Command;

class UpdateUser extends Command
{
    protected $signature = 'strava:update-user';

    protected $description = 'Command description';

    public function handle()
    {
        $user = User::find(1);

        $user->update([
            'strava_client_id' => config('services.strava.client_id'),
            'strava_client_secret' => config('services.strava.client_secret'),
            'strava_refresh_token' => config('services.strava.refresh_token'),
        ]);
    }
}
