<?php

namespace App\Console\Commands\Strava;

use Illuminate\Console\Command;
use App\User;

class AddActivity extends Command
{
    protected $signature = 'strava:add-activity';

    protected $description = 'Command description';

    public function handle()
    {
        $user = User::find(1);

        $job = new \App\Jobs\Strava\AddActivity(
            $user,
            now(),
            12.34,
            '1:3:23',
        );
        dispatch($job);
    }
}
