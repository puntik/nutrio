<?php

namespace App\Console\Commands;

use App\Model\User\WeightLog;
use App\Services\WeightEstimator;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class PredictWeightCommand extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zend:predict-weight
        {user_id}
        {--date= : Future date in format Y-m-d}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate weight prediction';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $userId    = $this->argument('user_id');
        $dateInput = $this->option('date');

        $date = ($dateInput === null) ?
            now()->addMonths(3) :
            Carbon::parse($dateInput);

        // prepare data
        $data = WeightLog::whereUserId($userId)
                         ->orderBy('weighed_at')
                         ->select(['weighed_at', 'weight'])
                         ->get();

        $regression = WeightEstimator::getRegression($data);

        $p = $regression->predict([$date->timestamp]);

        $this->line(sprintf('%.1fkg', $p));

        $startAt = $data->first()->weighed_at;
        $endAt   = $data->last()->weighed_at;

        $p1 = $regression->predict([$startAt->timestamp]);
        $p2 = $regression->predict([$endAt->timestamp]);

        $this->line(sprintf('Border %s: %.1f', $startAt->format('Y-m-d'), $p1));
        $this->line(sprintf('Border %s: %.1f', $endAt->format('Y-m-d'), $p2));

        return self::SUCCESS;
    }
}
