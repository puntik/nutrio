<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Events\MessageSending;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class LogSendingMessage implements ShouldQueue
{

    use InteractsWithQueue;

    public function handle(MessageSending $event)
    {
        Log::debug('About to send a new message ... ', $event->message->getTo());
    }
}
