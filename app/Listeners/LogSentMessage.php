<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Events\MessageSent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class LogSentMessage implements ShouldQueue
{

    use InteractsWithQueue;

    public function handle(MessageSent $event)
    {
        Log::debug('Sent a new message ... ', $event->message->getTo());
    }
}
