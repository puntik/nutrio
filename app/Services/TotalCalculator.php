<?php declare(strict_types = 1);

namespace App\Services;

class TotalCalculator
{

    public static function calculate(
        float $height,
        float $weight,
        float $age,
        string $gender,
        float $activityLevel = 0.5,
        float $stressLevel = 0.5
    ): float {
        $bmr = BmrCalculator::calculate($height, $weight, $age, $gender);

        return $bmr * (1 + $activityLevel * 0.5 + $stressLevel * 0.3);
    }
}
