<?php declare(strict_types = 1);

namespace App\Services;

class NutrientsCalculator
{

    private const CARBOHYDRATES_RATE = 4;

    private const PROTEINS_RATE = 4;

    private const FATS_RATE = 9;

    public const NORMAL = [
        'carbohydrates' => 55,
        'proteins'      => 15,
        'fats'          => 30,
    ];

    public function calculate($energy, array $target = self::NORMAL)
    {
        return [
            'carbohydrates' => [
                'energy' => round($target['carbohydrates'] * $energy / 100),
                'weight' => round(($target['carbohydrates'] * $energy / 100) / self::CARBOHYDRATES_RATE),
            ],
            'proteins'      => [
                'energy' => round($target['proteins'] * $energy / 100),
                'weight' => round(($target['proteins'] * $energy / 100) / self::PROTEINS_RATE),
            ],
            'fats'          => [
                'energy' => round($target['fats'] * $energy / 100),
                'weight' => round(($target['fats'] * $energy / 100) / self::FATS_RATE),
            ],
        ];
    }
}
