<?php declare(strict_types = 1);

namespace App\Services;

use Phpml\Regression\LeastSquares;

class WeightEstimator
{

    public static function getRegression($data): LeastSquares
    {
        $samples = [];
        $targets = [];

        foreach ($data as $item) {
            $samples[] = [$item->weighed_at->timestamp];
            $targets[] = $item->weight;
        }

        $r = new LeastSquares();
        $r->train($samples, $targets);

        return $r;
    }
}
