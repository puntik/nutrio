<?php declare(strict_types = 1);

namespace App\Services;

class WorkoutNoticeResolver
{

    public static function resolve(int $hour)
    {
        $timeDescription = match (true) {
            $hour >= 5 && $hour < 9 => 'Ranni trening',
            $hour >= 9 && $hour < 13 => 'Dopoledni trening',
            $hour >= 13 && $hour < 17 => 'Odpoledni trening',
            $hour >= 17 && $hour < 23 => 'Vecerni trening',
            default => 'Nocni trening'
        };

        return $timeDescription;
    }
}
