<?php declare(strict_types = 1);

namespace App\Services;

class BmiCalculator
{

    /**
     * @param mixed $height cm
     * @param mixed $weight kg
     * @param null  $age    years
     *
     * @return float
     */
    public static function calculate($height, $weight, $age = null): float
    {
        return $weight / (($height / 100) ^ 2);
    }
}
