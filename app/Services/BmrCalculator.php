<?php declare(strict_types = 1);

namespace App\Services;

class BmrCalculator
{

    /**
     * Implements Harris-Benedict formula
     *
     * @param float  $height cm
     * @param float  $weight kg
     * @param float  $age    year
     * @param string $gender M or F
     *
     * @return float
     */
    public static function calculate(
        float $height,
        float $weight,
        float $age,
        string $gender
    ): float {
        if ($gender === 'M') {
            return 66 + 13.7 * $weight + 5 * $height - 6.8 * $age;
        }

        return 655.0935 + 9.6 * $weight + 1.85 * $height - 4.7 * $age;
    }
}
