<?php
namespace App\Services;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class StatsService
{
    public function statsForUser(int $userId, int $days = 14)
    {
        $data = DB::table('users_workout_log_workout_groups')
            ->leftJoin('workout_groups', 'workout_groups.id', '=', 'users_workout_log_workout_groups.workout_groups_id')
            ->leftJoin(
                'users_workout_log',
                'users_workout_log.id',
                '=',
                'users_workout_log_workout_groups.users_workout_log_id'
            )
            ->where('user_id', $userId)
            ->whereDate('workout_at', '>', now()->subDays($days))
            ->groupBy('workout_groups.id')

            ->select(['workout_groups.id', DB::raw('count(*) as count')])
            ->get();

        return $data;
    }

    public function statsByDay(int $userId, int $days = 14)
    {
        $data = DB::table('users_workout_log')
            ->leftJoin(
                'users_workout_log_workout_groups',
                'users_workout_log.id',
                '=',
                'users_workout_log_workout_groups.users_workout_log_id'
            )
            ->where('user_id', $userId)
            ->whereDate('workout_at', '>', now()->subDays($days))
            ->orderBy('users_workout_log.workout_at')        
            ->select('users_workout_log.workout_at', 'users_workout_log_workout_groups.workout_groups_id' )
            ->get();     
            
        $byDays;
        foreach($data as $row) {
            $byDays[Carbon::parse($row->workout_at)->format('Y-m-d')][] = $row->workout_groups_id;

        }    

        return $byDays;
    }
}
