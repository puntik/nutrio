<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{

    protected $table = 'activities';

    protected $fillable = [
        'title',
        'energy' // kcal in minute per kilogram
    ];
}
