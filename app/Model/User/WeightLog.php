<?php

namespace App\Model\User;

use App\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WeightLog extends Model
{

    use HasFactory;

    protected $table = 'users_weight_log';

    protected $casts = [
        'user_id' => 'integer',
        'weight'  => 'float',
        'created_at' => 'datetime',
        'weighed_at' => 'datetime'
    ];

    protected $fillable = [
        'user_id',
        'weight',
        'weighed_at',
    ];

    // relations

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
