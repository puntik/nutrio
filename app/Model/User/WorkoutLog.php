<?php

namespace App\Model\User;

use App\Model\WorkoutGroup;
use App\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WorkoutLog extends Model
{

    use HasFactory;

    protected $table = 'users_workout_log';

    protected $fillable = [
        'notice',     // poznamka k treningu
        'user_id',    // uzivatel
        'workout_at', // kdy jsem cvicil
    ];

    protected $casts = [
        'user_id'    => 'integer',
        'workout_at' => 'datetime',
    ];

    // relations

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function groups()
    {
        return $this->belongsToMany(
            WorkoutGroup::class,
            'users_workout_log_workout_groups',
            'users_workout_log_id',
            'workout_groups_id'
        );
    }
}
