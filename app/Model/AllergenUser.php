<?php declare(strict_types = 1);

namespace App\Model;

use Illuminate\Database\Eloquent\Relations\Pivot;

class AllergenUser extends Pivot
{

    protected $casts = [
        'importance' => 'integer',
    ];
}
