<?php declare(strict_types = 1);

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Allergen extends Model
{

    protected $fillable = [
        'title',
        'description',
        'code',
    ];

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class)
                    ->using(AllergenUser::class)
                    ->withPivot('importance');
    }
}
