<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Diet extends Model
{

    protected $table = 'diets';

    protected $casts = [
        'carbohydrates' => 'integer',
        'proteins'      => 'integer',
        'fats'          => 'integer',
    ];

    protected $fillable = [
        'title',
        'description',
        'carbohydrates',
        'proteins',
        'fats',
    ];
}
