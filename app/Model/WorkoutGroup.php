<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class WorkoutGroup extends Model
{

    protected $table = 'workout_groups';

    protected $fillable = [
        'title',
        'description',
    ];
}
