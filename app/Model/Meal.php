<?php declare(strict_types = 1);

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Meal extends Model
{

    protected $fillable = [
        'title',
        'energy',
        'proteins',
        'fats',
        'fatty_acid',
        'carbohydrates',
        'sugars',
        'fibre',
        'salt',
    ];

    protected $casts = [
        'energy'        => 'float',
        'proteins'      => 'float',
        'fats'          => 'float',
        'carbohydrates' => 'float',
    ];

    public function allergens(): BelongsToMany
    {
        return $this->belongsToMany(Allergen::class);
    }

    public function ingredients(): BelongsToMany
    {
        return $this->belongsToMany(Ingredient::class);
    }
}
