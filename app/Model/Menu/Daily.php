<?php declare(strict_types = 1);

namespace App\Model\Menu;

use App\Model\Meal;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Daily extends Model
{

    protected $table = 'dailies';

    protected $fillable = [
        'user_id',
        'applied_at',
    ];

    protected $casts = [
        'user_id' => 'integer',
    ];

    protected $dates = [
        'applied_at',
    ];

    // relations

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function meals()
    {
        return $this
            ->belongsToMany(Meal::class)
            ->using(DailyMeal::class);
    }
}
