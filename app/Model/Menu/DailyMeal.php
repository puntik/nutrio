<?php declare(strict_types = 1);

namespace App\Model\Menu;

use Illuminate\Database\Eloquent\Relations\Pivot;

class DailyMeal extends Pivot
{

    protected $casts = [
        'daily_type' => 'integer',
    ];
}
