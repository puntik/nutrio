<?php declare(strict_types = 1);

namespace App\Model\Menu;

class DailyType
{

    public const BREAKFAST = 100;

    public const LUNCH = 500;

    public const DINNER = 900;
}
