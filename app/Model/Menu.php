<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{

    protected $fillable = [
        'title',
    ];

    // relations
    public function meals()
    {
        return $this->belongsToMany(Meal::class)
                    ->withPivot('amount');
    }
}
