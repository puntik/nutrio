<?php

namespace App\Http\Controllers;

use App\Jobs\UpdateWeightJob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{

    public function edit(Request $request)
    {
        $user = \App\User::find(Auth::id());

        return view('user.profile', compact('user'));
    }

    public function store(Request $request)
    {
        $validated = $this->validate($request, [
            'gender'  => 'required|in:M,F',
            'born_at' => 'required|date',
            'height'  => 'required',
            'weight'  => 'required',
        ]);

        $user = Auth::user();

        $user->update($validated);

        dispatch(new UpdateWeightJob(
            $user,
            $validated['weight'],
            now()
        ));

        return to_route('dashboard');
    }
}
