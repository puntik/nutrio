<?php declare(strict_types = 1);

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\WeightLog as WeightLogResource;
use App\Model\User\WeightLog;
use Illuminate\Http\Request;
use App\Services\BmiCalculator;
use App\Services\BmrCalculator;
use App\Services\WeightEstimator;
use App\User;
use Illuminate\Support\Facades\Cache;

class WeightController extends Controller
{
    private const CACHE_TTL = 3600;

    public function __invoke(Request $request)
    {
        $weights = WeightLog::whereUserId($request->user()->id)
                            ->orderBy('weighed_at')
                            ->select([
                                'weighed_at',
                                'weight',
                            ])->get();

        return WeightLogResource::collection($weights);
    }

    public function info(Request $request)
    {
        $user     = $request->user();
        $cacheKey = sprintf('user:dashboard:%d', $user->id);

        $data = Cache::remember($cacheKey, self::CACHE_TTL, function () use ($user) {
            return $this->prepareDashboardData($user);
        });

        return $data;
    }

    private function prepareDashboardData(User $user)
    {
        // for table
        $wl = WeightLog::whereUserId($user->id)
                       ->orderByDesc('weighed_at')
                       ->take(7)
                       ->select(['weight', 'weighed_at'])
                       ->get();

        // for regression
        $wlR = WeightLog::whereUserId($user->id)
                        ->orderBy('weighed_at')
                        ->select(['weight', 'weighed_at'])
                        ->get();

        $prev = WeightLog::whereUserId($user->id)
                         ->where('weighed_at', '<', now()->subMonth())
                         ->orderByDesc('weighed_at')
                         ->first(['weight', 'weighed_at']);

        $last = $wl->first();

        // some prediction
        // za tri mesice
        try {
            $regression = WeightEstimator::getRegression($wlR);

            $predictionDate   = now()->addMonths(3);
            $predictionWeight = $regression->predict([$predictionDate->timestamp]);
        } catch (\Throwable $e) {
            $predictionDate   = now()->addMonths(3);
            $predictionWeight = null; // $regression->predict([$predictionDate->timestamp]);
        }

        return [
            'weight_log' => $wl,

            'weight'     => number_format($last->weight, 1),
            'weighed_at' => $last->weighed_at->format('Y-m-d'),

            'prev_weight'     => $prev?->weight,
            'prev_weighed_at' => $prev?->weighed_at->format('Y-m-d'),
            'diff'            => number_format($prev?->weight - $last->weight, 1),

            'prediction_weight' => number_format($predictionWeight, 1),
            'prediction_at'     => $predictionDate->format('Y-m-d'),

            'age' => $user->age,
            'bmi' => number_format(BmiCalculator::calculate($user->height, $last->weight), 1),
            'bmr' => number_format(BmrCalculator::calculate(
                $user->height,
                $last->weight,
                $user->age,
                $user->gender
            )),
        ];
    }
}
