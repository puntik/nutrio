<?php declare(strict_types = 1);

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\Activity;

class ActivitiesController extends Controller
{

    public function __invoke()
    {
        return Activity::all(['id', 'title', 'energy']);
    }
}
