<?php declare(strict_types = 1);

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Workout;
use App\Jobs\CreateWorkoutLog;
use App\Jobs\Strava\AddActivity;
use App\Model\User\WorkoutLog;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Log;

class WorkoutController extends Controller
{

    public function index()
    {
        $workouts = WorkoutLog::whereUserId(Auth::id())
                              ->whereDate('workout_at', '>=', now()->subWeeks(2))
                              ->orderByDesc('workout_at')
                              ->get();

        return Workout::collection($workouts);
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'workout_at'          => 'required|date',
            'groups'              => 'required|array',
            'strava_save'         => 'sometimes|required|accepted',
            'strava_distance'     => 'required_if:strava_save,accepted',
            'strava_elapsed_time' => 'required_if:strava_save,accepted',
        ]);

        dispatch(new CreateWorkoutLog(
            Auth::user()->id,
            $validated['groups'],
            Carbon::parse($validated['workout_at']),
        ));

        Log::debug($validated);
        Log::debug($request->all());

        if (config('services.strava.enabled') && Arr::has($validated, 'strava_save') && $validated['strava_save']) {
            dispatch(new AddActivity(
                Auth::user(),
                Carbon::parse($validated['workout_at']),
                (float) $validated['strava_distance'],
                $validated['strava_elapsed_time']
            ));
        }

        return [
            'notice' => \App\Services\WorkoutNoticeResolver::resolve(Carbon::parse($validated['workout_at'])->hour),
        ];
    }
}
