<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\BmiCalculator;
use App\Services\BmrCalculator;
use App\Services\TotalCalculator;
use Illuminate\Http\Request;

class BmrController extends Controller
{

    public function __invoke(
        Request $request
    ): array {
        $validated = $request->validate([
            'gender'         => 'required|in:M,F',
            'weight'         => 'required|numeric',
            'height'         => 'required|numeric',
            'age'            => 'required|numeric',
            'activity_level' => 'sometimes|numeric|min:0|max:1',
            'stress_level'   => 'sometimes|numeric|min:0|max:1',
        ]);

        return [
            'bmr' => sprintf('%2.1f', BmrCalculator::calculate(
                $validated['height'],
                $validated['weight'],
                $validated['age'],
                $validated['gender']
            )),

            'bmi' => sprintf('%2.1f', BmiCalculator::calculate(
                $validated['height'],
                $validated['weight']
            )),

            'total' => sprintf('%2.1f', TotalCalculator::calculate(
                $validated['height'],
                $validated['weight'],
                $validated['age'],
                $validated['gender'],
                $validated['activity_level'] ?? 0,
                $validated['stress_level'] ?? 0
            )),
        ];
    }
}
