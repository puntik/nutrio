<?php declare(strict_types = 1);

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\Allergen;

class AllergensController extends Controller
{

    public function __invoke()
    {
        return Allergen::all(['id', 'title']);
    }
}
