<?php declare(strict_types = 1);

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\Meal;
use Illuminate\Http\Request;

class MealsController extends Controller
{

    public function index(Request $request)
    {
        $meals = Meal::getQuery();

        if ($request->has('title')) {
            $meals->where('title', 'LIKE', '%' . $request->get('title') . '%');
            $meals->limit(12);
        }

        return $meals->get();
    }

    public function show(int $id)
    {
        return Meal::findOrFail($id);
    }
}
