<?php declare(strict_types = 1);

namespace App\Http\Controllers\Api\Workout;

use App\Http\Controllers\Controller;
use App\Model\User\WorkoutLog;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class CalendarController extends Controller
{

    private const CACHE_TTL = 3600;

    public function __invoke()
    {
        return Cache::remember(sprintf('workout-calendar:%d', Auth::id()), self::CACHE_TTL, function () {
            $startDate = new Carbon('2022-11-14');

            return WorkoutLog::where('workout_at', '>=', $startDate)
                    ->orderBy('workout_at', 'asc')
                    ->get(['workout_at'])
                ->map(function (WorkoutLog $workout) use ($startDate) {
                    return $workout->workout_at->format('Y-m-d');
                })->unique()->sort()->values();
        });
    }
}
