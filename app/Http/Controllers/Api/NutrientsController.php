<?php declare(strict_types = 1);

namespace App\Http\Controllers\Api;

use App\Services\NutrientsCalculator;
use Illuminate\Http\Request;

class NutrientsController extends \App\Http\Controllers\Controller
{

    /** @var NutrientsCalculator */
    private $calc;

    public function __construct()
    {
        $this->calc = new NutrientsCalculator();
    }

    public function __invoke(Request $request)
    {
        $validated = $this->validate($request, [
            'energy' => 'required|numeric|min:800|max:4200',
        ]);

        return $this->calc->calculate($validated['energy']);
    }
}
