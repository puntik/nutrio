<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BmiController extends Controller
{

    public function __invoke(Request $request)
    {
        return view('bmr');
    }
}
