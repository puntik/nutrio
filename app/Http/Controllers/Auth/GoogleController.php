<?php declare(strict_types = 1);

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Laravel\Socialite\Facades\Socialite;

class GoogleController extends Controller
{

    private const AUTH_DRIVER = 'google';

    public function redirect()
    {
        return Socialite::driver(self::AUTH_DRIVER)->redirect();
    }

    public function callback()
    {
        $googleUser = Socialite::driver(self::AUTH_DRIVER)->user();

        try {
            $user = User::whereEmail($googleUser->getEmail())->firstOr(function () use ($googleUser) {
                throw new \InvalidArgumentException(sprintf('There is no such user "%s"', $googleUser->getEmail()));
            });

            Auth::login($user);
        } catch (\Throwable $e) {
            Log::notice($e->getMessage());

            return redirect('/login');
        }

        return redirect('/');
    }
}
