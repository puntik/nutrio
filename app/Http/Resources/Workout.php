<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Workout extends JsonResource
{

    public function toArray($request)
    {
        /** @var \App\Model\User\WorkoutLog $that */
        $that = $this;

        return [
            'id'         => $that->id,
            'workout_at' => $that->workout_at->format('Y-m-d'),
            'notice'     => $that->notice,
        ];
    }
}
