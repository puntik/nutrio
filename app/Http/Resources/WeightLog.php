<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class WeightLog extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        /** @var \App\Model\User\WeightLog $that */
        $that = $this;

        return [
            'weighed_at' => $that->weighed_at->format('Y-m-d'),
            'weight'     => $that->weight,
        ];
    }
}
