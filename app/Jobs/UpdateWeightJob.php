<?php

namespace App\Jobs;

use App\Model\User\WeightLog;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class UpdateWeightJob implements ShouldQueue
{

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private User   $user;
    private float  $weight;
    private Carbon $weighedAt;

    public function __construct(
        User $user,
        float $weight,
        Carbon $weighedAt
    ) {

        $this->user      = $user;
        $this->weight    = $weight;
        $this->weighedAt = $weighedAt;
    }

    public function handle()
    {
        DB::transaction(function () {
            WeightLog::create([
                'user_id'    => $this->user->id,
                'weight'     => $this->weight,
                'weighed_at' => $this->weighedAt,
            ]);

            $this->user->update([
                'weight' => $this->weight,
            ]);

            Cache::delete(sprintf('user:dashboard:%d', $this->user->id));
        });
    }
}
