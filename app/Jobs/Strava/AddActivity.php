<?php

namespace App\Jobs\Strava;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

class AddActivity implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct(
        private User $user,
        readonly private \DateTime $startDateLocal,
        readonly private float $distance,
        readonly private string $elapsedTime,
        readonly private string $type = 'Run',
        readonly private string $sportType = 'Run',
    ) {
    }

    public function handle(): void
    {
        $parsed = date_parse_from_format('H:i:s', $this->elapsedTime);
        $sec = $parsed['hour'] * 60 * 60 + $parsed['minute'] * 60 + $parsed['second'];

        // type a sport_type na ciselniky

        $activity = [
            'name' => 'Morning run',
            'type' => $this->type,                       // 'Run',
            'sport_type' => $this->sportType,            // 'Run',       // Run, Trial Run, Walk, Hike, Swim
            'start_date_local' => $this->startDateLocal->format(DATE_RFC3339),
            'distance' => $this->distance * 1000,
            'elapsed_time' => $sec,
            'description' => '',
            'commute' => 0
        ];

        Log::debug($activity);
    }
}
