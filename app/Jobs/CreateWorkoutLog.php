<?php

namespace App\Jobs;

use App\Model\User\WorkoutLog;
use App\Model\WorkoutGroup;
use App\Services\WorkoutNoticeResolver;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CreateWorkoutLog implements ShouldQueue
{

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private int    $userId;
    private array  $groups;
    private Carbon $workoutAt;

    public function __construct(
        int $userId,
        array $groups,
        Carbon $workoutAt
    ) {
        $this->userId    = $userId;
        $this->groups    = $groups;
        $this->workoutAt = $workoutAt;
    }

    public function handle()
    {
        $notice = $this->processNotice(
            $this->groups,
            $this->workoutAt
        );

        DB::transaction(function () use ($notice) {
            $log = WorkoutLog::create(
                [
                    'notice'     => $notice,
                    'user_id'    => $this->userId,
                    'workout_at' => $this->workoutAt,
                ]
            );

            $log->groups()->attach($this->groups);
        });
    }

    private function processNotice(array $groups, Carbon $workoutAt): string
    {
        $groupsText = WorkoutGroup::whereIn('id', $groups)
                                  ->get(['title'])
                                  ->map(function (WorkoutGroup $group) {
                                      return Str::lower($group->title);
                                  })
                                  ->join(', ');

        $timeDescription = WorkoutNoticeResolver::resolve($workoutAt->hour);

        return sprintf('%s (%s)', $timeDescription, $groupsText);
    }
}
