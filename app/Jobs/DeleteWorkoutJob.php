<?php

namespace App\Jobs;

use App\Model\User\WorkoutLog;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class DeleteWorkoutJob implements ShouldQueue
{

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private int $userId;
    private int $workoutId;

    public function __construct(int $userId, int $workoutId)
    {
        $this->userId    = $userId;
        $this->workoutId = $workoutId;
    }

    public function handle()
    {
        $workout = WorkoutLog::where([
            'id'      => $this->workoutId,
            'user_id' => $this->userId,
        ])->firstOrFail();

        DB::transaction(function () use ($workout) {
            // delete groups
            $workout->groups()->detach();

            // delete workout
            $workout->delete();
        });
    }
}
