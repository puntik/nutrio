<?php

namespace App;

use App\Model\Allergen;
use App\Model\AllergenUser;
use App\Model\User\WeightLog;
use App\Services\BmiCalculator;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{

    use HasApiTokens, Notifiable;

    protected $fillable = [
        'name',
        'email',
        'password',
        'gender',
        'born_at',
        'weight',
        'height',
        'strava_client_id',
        'strava_client_secret',
        'strava_refresh_token',
        'strava_access_token',
        'strava_scope',
    ];

    protected $hidden = [
        'password',
        'remember_token',
        'strava_client_secret',
        'strava_refresh_token',
        'strava_access_token',
    ];

    protected $casts = [
        'born_at'           => 'date',
        'email_verified_at' => 'datetime',
        'weight'            => 'decimal:1',
        'bmi'               => 'decimal:1',
    ];

    // relations

    public function allergens(): BelongsToMany
    {
        return $this->belongsToMany(Allergen::class)
                    ->using(AllergenUser::class)
                    ->withPivot('importance');
    }

    public function weights()
    {
        return $this->hasMany(WeightLog::class)->orderByDesc('weighed_at');
    }

    // attributes

    public function age(): Attribute
    {
        return new Attribute(
            get: fn($value, $attributes) => now()->diffInYears($attributes['born_at'])
        );
    }

    public function bmi(): Attribute
    {
        return new Attribute(
            get: fn($value, $attributes) => BmiCalculator::calculate(
                $attributes['height'],
                $attributes['weight'],
                $this->age
            )
        );
    }
}
