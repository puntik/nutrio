<?php

namespace Database\Factories;

use App\Model\Meal;
use Illuminate\Database\Eloquent\Factories\Factory;

class MealFactory extends Factory
{

    protected $model = Meal::class;

    public function definition()
    {
        return [
            'title'         => $this->faker->sentence,
            'energy'        => $this->faker->randomFloat(0, 120, 1200),
            'proteins'      => 0,
            'fats'          => 0,
            'fatty_acid'    => 0,
            'carbohydrates' => 0,
            'sugars'        => 0,
            'fibre'         => 0,
            'salt'          => 0,
        ];
    }
}
