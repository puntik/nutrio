<?php

namespace Database\Factories;

use App\Model\Activity;
use Illuminate\Database\Eloquent\Factories\Factory;

class ActivityFactory extends Factory
{

    protected $model = Activity::class;

    public function definition()
    {
        return [
            'title'  => $this->faker->sentence(2),
            'energy' => $this->faker->randomFloat(null, 0.05, 0.2),
        ];
    }
}
