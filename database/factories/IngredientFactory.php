<?php

namespace Database\Factories;

use App\Model\Ingredient;
use Illuminate\Database\Eloquent\Factories\Factory;

class IngredientFactory extends Factory
{

    protected $model = Ingredient::class;

    public function definition()
    {
        return [
            'title' => $this->faker->word,
        ];
    }
}
