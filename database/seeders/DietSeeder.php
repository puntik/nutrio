<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DietSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('diets')->insert([
            'title'         => 'Normal',
            'description'   => 'Základní trojpoměr živin',
            'carbohydrates' => 55,
            'proteins'      => 15,
            'fats'          => 30,
        ]);
    }
}
