<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ActivitySeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('activities')->insert([
            [
                'title'  => 'Běh 10km/h',
                'energy' => 0.176,
            ],
            [
                'title'  => 'Chůze 4km/h po rovině',
                'energy' => 0.052,
            ],
            [
                'title'  => 'Chůze 6km/h',
                'energy' => 0.095,
            ],
            [
                'title'  => 'Jízda na kole 19km/h',
                'energy' => 0.125,
            ],
            [
                'title'  => 'Plavání',
                'energy' => 0.162,
            ],
            [
                'title'  => 'Horská turistika',
                'energy' => 0.149,
            ],
        ]);
    }
}
