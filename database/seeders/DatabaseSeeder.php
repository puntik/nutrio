<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            ActivitySeeder::class,
            AllergensSeeder::class,
            DietSeeder::class,
            UsersSeeder::class,
            WorkoutGroupSeeder::class,
        ]);
    }
}
