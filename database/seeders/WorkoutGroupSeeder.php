<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WorkoutGroupSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('workout_groups')->insert([
            [
                'title' => 'Biceps',
            ],
            [
                'title' => 'Bricho',
            ],
            [
                'title' => 'Kardio',
            ],
            [
                'title' => 'Nohy',
            ],
            [
                'title' => 'Prsa',
            ],
            [
                'title' => 'Ramena',
            ],
            [
                'title' => 'Triceps',
            ],
            [
                'title' => 'Zada',
            ],
        ]);
    }
}
