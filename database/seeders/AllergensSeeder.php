<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AllergensSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('allergens')->insert([
            [
                'title' => 'Lepek',
                'code'  => 1,
            ],
            [
                'title' => 'Korýši',
                'code'  => 2,
            ],
            [
                'title' => 'Vejce',
                'code'  => 3,
            ],
            [
                'title' => 'Ryby',
                'code'  => 4,
            ],
            [
                'title' => 'Arašídy',
                'code'  => 5,
            ],
            [
                'title' => 'Sója',
                'code'  => 6,
            ],
            [
                'title' => 'Mléko',
                'code'  => 7,
            ],
            [
                'title' => 'Skořápkové plody',
                'code'  => 8,
            ],
            [
                'title' => 'Celer',
                'code'  => 9,
            ],
            [
                'title' => 'Hořčice',
                'code'  => 10,
            ],
            [
                'title' => 'Sezam',
                'code'  => 11,
            ],
            [
                'title' => 'Oxid siřičitý a siřičitany',
                'code'  => 12,
            ],
            [
                'title' => 'Vlčí bob',
                'code'  => 13,
            ],
            [
                'title' => 'Měkkýši',
                'code'  => 14,
            ],
        ]);
    }
}
