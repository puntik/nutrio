<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'              => 'Admin',
            'email'             => 'admin@localhost',
            'email_verified_at' => now(),
            'password'          => Hash::make('secret'),
            'created_at'        => now(),
            'updated_at'        => now(),
            'gender'            => 'M',
            'born_at'           => now()->subYears(33),
            'weight'            => 173,
            'height'            => 72,
        ]);
    }
}
