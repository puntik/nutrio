<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('strava_client_id')->nullable();
            $table->string('strava_client_secret')->nullable();
            $table->string('strava_refresh_token')->nullable();
            $table->string('strava_access_token')->nullable();
            $table->string('strava_scope')->nullable();
            $table->timestamp('strava_connected_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn([
                'strava_client_id',
                'strava_client_secret',
                'strava_refresh_token',
                'strava_access_token',
                'strava_scope',
                'strava_connected_at',
            ]);
        });
    }
};
