<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDailyMenuTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_menu', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('daily_id');
            $table->unsignedBigInteger('meal_id');

            $table->unsignedInteger('daily_type')->default(500);

            $table->timestamps();

            $table->foreign('daily_id', 'fx_daily_meal_daily_id_dailies_id')
                  ->references('id')
                  ->on('dailies');

            $table->foreign('meal_id', 'fx_daily_meal_meal_id_meals_id')
                  ->references('id')
                  ->on('meals');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daily_menu');
    }
}
