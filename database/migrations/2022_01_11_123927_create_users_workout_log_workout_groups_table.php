<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersWorkoutLogWorkoutGroupsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_workout_log_workout_groups', function (Blueprint $table) {
            $table->unsignedBigInteger('users_workout_log_id');
            $table->unsignedBigInteger('workout_groups_id');

            $table->primary([
                'users_workout_log_id',
                'workout_groups_id',
            ]);

            $table->foreign(
                'users_workout_log_id',
                'fx_users_workout_log_workout_groups_users_workout_log_id_users_workout_log_id')
                  ->references('id')
                  ->on('users_workout_log');

            $table->foreign(
                'workout_groups_id',
                'fx_users_workout_log_workout_groups_workout_groups_id_workout_groups_id')
                  ->references('id')
                  ->on('workout_groups');
        });
    }

    // fx_{table}_{column}_{table}_{column}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_workout_log_workout_groups');
    }
}
