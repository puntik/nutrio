<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableAllergenUser extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('allergen_user', function (Blueprint $table) {
            $table->unsignedBigInteger('allergen_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedTinyInteger('importance')->default(5);

            $table->foreign('allergen_id', 'fx_allergen_user_allergen_id_allergens_id')
                  ->references('id')
                  ->on('allergens');

            $table->foreign('user_id', 'fx_allergen_user_user_id_users_id')
                  ->references('id')
                  ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('allergen_user');
    }
}
