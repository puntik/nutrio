<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAllergenMealTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('allergen_meal', function (Blueprint $table) {
            $table->unsignedBigInteger('allergen_id');
            $table->unsignedBigInteger('meal_id');

            $table->foreign('allergen_id', 'fx_allergen_meal_allergen_id_allergens_id')
                  ->references('id')
                  ->on('allergens');

            $table->foreign('meal_id', 'fx_allergen_meal_meal_id_meals_id')
                  ->references('id')
                  ->on('meals');

            $table->unique(['allergen_id', 'meal_id'], 'ux_allergen_meal_allergen_id_meal_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('allergen_meal');
    }
}
