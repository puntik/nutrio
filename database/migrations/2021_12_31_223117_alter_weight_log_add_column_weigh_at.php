<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterWeightLogAddColumnWeighAt extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_weight_log', function (Blueprint $table) {
            $table->dateTime('weighed_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_weight_log', function (Blueprint $table) {
            $table->dropColumn('weighed_at');
        });
    }
}
