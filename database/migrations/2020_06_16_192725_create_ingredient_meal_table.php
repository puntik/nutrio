<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIngredientMealTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingredient_meal', function (Blueprint $table) {
            $table->unsignedBigInteger('ingredient_id');
            $table->unsignedBigInteger('meal_id');

            $table->foreign('ingredient_id', 'fx_ingredient_id_ingredients_id')->references('id')->on('ingredients');
            $table->foreign('meal_id', 'fx_meal_id_meals_id')->references('id')->on('meals');
            $table->primary(['ingredient_id', 'meal_id'], 'pk_ingredient_meal_ingredient_id_meal_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ingredient_meal');
    }
}
