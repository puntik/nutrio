<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMealMenuTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meal_menu', function (Blueprint $table) {
            $table->unsignedBigInteger('meal_id');
            $table->unsignedBigInteger('menu_id');
            $table->float('amount')->default(0);

            $table->primary(['meal_id', 'menu_id']);
            $table->foreign('meal_id', 'fx_meal_menu_meal_id_meals_id')->references('id')->on('meals');
            $table->foreign('menu_id', 'fx_meal_menu_menu_id_menu_id')->references('id')->on('menus');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meal_menu');
    }
}
