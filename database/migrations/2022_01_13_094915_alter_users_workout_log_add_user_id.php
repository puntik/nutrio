<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterUsersWorkoutLogAddUserId extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_workout_log', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id');

            $table->foreign('user_id', 'fx_users_workout_log_user_id_users_id')
                  ->references('id')
                  ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_workout_log', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });
    }
}
