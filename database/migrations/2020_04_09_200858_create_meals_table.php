<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMealsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meals', function (Blueprint $table) {
            $table->id();

            $table->string('title', 256);
            $table->float('energy', 6, 2);
            $table->float('fats', 6, 2);
            $table->float('fatty_acid', 6, 2);
            $table->float('carbohydrates', 6, 2);
            $table->float('sugars', 6, 2);
            $table->float('fibre', 6, 2);
            $table->float('salt', 6, 2);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meals');
    }
}
