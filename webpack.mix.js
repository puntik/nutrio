const tailwindcss = require('tailwindcss')

const mix = require('laravel-mix');

mix.ts("resources/js/app.tsx", "public/js").postCss("resources/css/app.css", "public/css", [
    require("tailwindcss"),
]).react();

mix.browserSync({
    proxy: process.env.APP_URL,
    files: [
        './resources/css/*',
        './resources/js/react/**/*',
        './resources/views/**/*.blade.php',
    ]
});

mix.disableSuccessNotifications();
