require('./bootstrap');

import 'flowbite';
import React from "react";
import { createRoot } from 'react-dom/client';

// import { App } from "./react/App";

import {
    createBrowserRouter,
    RouterProvider,
} from "react-router-dom";

import { LineChart } from './react/weight/LineChart';
import { List } from './react/workout/List';

const router = createBrowserRouter([
    {
        path: "/chartjs",
        element: <LineChart />,
    },
    {
        path: "/workout",
        element: <List />,
    },
]);

const appDiv = document.getElementById("main-app")!;

const root = createRoot(appDiv);

root.render(
    <React.StrictMode>
        <RouterProvider router={router} />
    </React.StrictMode>
);
