import React, { useEffect, useState } from "react"
import { Line } from "react-chartjs-2"
import { Link } from "react-router-dom"

import {Chart as ChartJS, TimeScale} from 'chart.js/auto';
import axios from "axios";

 ChartJS.register(
     TimeScale
 );

 const Table: React.FC<{data: ( string | number )[][]}> = ({data =[]}) => {
        return (
            <table className="table table-auto w-full">
                <tbody>
                {data.map((item) => {
                    return (<tr className="odd:bg-teal-50">
                        <td className="border px-4 py-2 text-sm">{item[0]}</td>
                        <td className="border px-4 py-2 text-sm">{item[1]}kg</td></tr>)
                })}
                </tbody>
            </table>
        )
  }

const LineWrapper: React.FC<{data: ( string | number )[][]}> = ({data = []}) => {
    const options = {
        responsive: true,
        plugins: {
          legend: {
            position: 'top' as const,
          },
          title: {
            display: true,
            text: 'Váhové změny v čase',
          },
        },
        scales: {
            y: {
                beginAtZero: false,
            },
        }
    };

    return (
        <Line
            id="hello"
            options={options}
            datasetIdKey='id'
            data={{
                datasets: [{
                    label: 'Váha [kg]',
                    data: data,
                    backgroundColor: '#00dd00',
                    borderColor: 'rgb(75, 192, 192)',
                    borderWidth: 2,
                    tension: 0.1,
                }],
            }}
            />
    )
};

const InfoBox: React.FC<{title?: string, body: string, notice?: string}> = ({title = '', body = '', notice = ''}) => {
    return (
        <div className="md:flex-1 border-0 border-t-4 border-green-200 bg-white bg-gradient-to-br from-green-100 via-white p-4 border rounded shadow">
            <h2 className="text-sm text-gray-500">{title}</h2>
            <span className="w-full block py-4 text-center text-gray-600 text-5xl font-bold">{body}</span>
            <div className="text-sm text-gray-500">
                {notice}
            </div>
        </div>


    );
}

const LineChart: React.FC = () => {
    const [data, setData] = useState<( string | number )[][]>([]);
    const [info, setInfo] = useState<any>({});

    const axiosData = async (): Promise<void> => {
        const response = await axios.get('/api/weights');
        const data = response.data.data.map((item: { weighed_at: any; weight: number; }) => {
            return [item.weighed_at, item.weight as number]
        });
        setData(data);

        const fetchedInfo = await axios.get('/api/weights/info');
        const info = fetchedInfo.data;
        setInfo(info);
    };

    useEffect(() => {
        axiosData().catch((e) => {
            console.log(e);
        });
    }, []);

    return (
        <>
            <Link to="/workout">Back to workouts</Link>
            <div className="flex space-x-2 mb-2">
                <InfoBox body={info.weight} title="Aktuální váha" notice="změna 23 kg od 12.3."></InfoBox>
                <InfoBox body={info.prediction_weight} title="Predikovaná váha" notice="predikce na tři měsíce"></InfoBox>
                <InfoBox body={info.bmi} title="Body mass index" notice="mírná nadváha"></InfoBox>
                <InfoBox body={info.bmr} title="Bazální kalorická spotřeba" notice=""></InfoBox>
            </div>
            <Table data={data} />
            <LineWrapper data={data} />
        </>
    )
}

export {
    LineChart
}