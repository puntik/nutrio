import React from "react";

const Alert: React.FC<any> = (props) => {
    return (
        <div role="alert" className="w-full flex justify-center my-2 p-2 bg-yellow-500 border border-yellow-600 rounded-sm shadow text-yellow-50 font-bold">
            {props.text}
        </div>
    )
}

export { Alert };