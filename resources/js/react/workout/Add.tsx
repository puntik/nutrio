import React from "react"
import { useState } from "react"

import { format } from 'date-fns';
import { groups } from "./groups";
import { Field, FieldArray, Form, Formik } from "formik";

interface InitialValues {
    id: number;
    notice: string;
    workout_at: string;
    groups: number[];
}

const DATE_FORMAT = "y-MM-dd'T'HH:00:00.00"

export const Add: React.FC<any> = (props) => {

    const [hiddenForm, setHiddenForm] = useState(
        true
    )

    const toogleForm = () => {
        setHiddenForm(!hiddenForm)
    }

    const initialValues: InitialValues = {
        id: 0,
        notice: 'Nový tréning',
        workout_at: format(new Date(), DATE_FORMAT),
        groups: [],
    };

    const onSubmit = (values: any, actions: any) => {
        props.onWorkoutAdded(values);
        actions.resetForm({});
    }

    const onReset = (actions: any) => {
        toogleForm()
    }

    return (<>
            {hiddenForm && (
                <>
                    <button onClick={toogleForm} className="w-full flex justify-center bg-teal-50 hover:bg-teal-100 py-3 rounded shadow">
                        <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4 text-gray-400" fill="none" viewBox="0 0 24 24"
                             stroke="currentColor">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                                  d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z"/>
                        </svg>
                    </button>
                </>
            )}

            {!hiddenForm && (
                <>
                    <Formik
                    initialValues={initialValues}
                    onSubmit={onSubmit}
                    onReset={onReset}>
                    {({values}) => (
                        <Form className="space-y-1">
                        <div>
                            <Field type="datetime-local" name="workout_at" className="p-2" />
                        </div>
                        <FieldArray
                            name="groups"
                            render={(arrayHelpers: any) => (
                                <div className="grid grid-cols-3">
                                    {groups.map((group, index) => (
                                        <label 
                                            key={group.id} 
                                            className="space-x-2 p-3">
                                        <input
                                            key={index}
                                            type="checkbox"
                                            name="groups"
                                            className=""
                                            value={group.id}
                                            checked={values.groups.includes(group.id)}
                                            onChange={e => {
                                                if(e.target.checked) {
                                                    arrayHelpers.push(group.id)
                                                } else {
                                                    const idx = values.groups.indexOf(group.id);
                                                    arrayHelpers.remove(idx);
                                                }
                                            }}
                                        />
                                        <span className="">{group.title}</span>
                                        </label>
                                    ))}
                                </div>
                        )}/>

                        {/* <div>
                            <div>
                                <label htmlFor="strava_type" className="block text-sm text-gray-600">Logovat kardio do stravy:</label>
                                <select name="strava_type" id="strava_type">
                                    <option>Nelogovat</option>
                                    <option>Run</option>
                                    <option>Workout run</option>
                                    <option>Walk</option>
                                </select>
                            </div>

                            <div>
                                <label htmlFor="strava_elapsed_time" className="block text-sm text-gray-600">Vzdálenost:</label>
                                <input type="text" name="strava_elapsed_time" id="strava_elapsed_time" className="px-1 py-2 text-sm border border-gray-200 rounded shadow-inner"/>
                            </div>

                            <div>
                                <label htmlFor="strava_distance" className="block text-sm text-gray-600">Čas:</label>
                                <input type="text" name="strava_distance" id="strava_distance" className="px-1 py-2 text-sm border border-gray-200 rounded shadow-inner"/>
                            </div>
                        </div> */}

                        <div className="flex space-x-2 my-2 items-center">
                            <button type="submit" className="flex-1 bg-teal-600 hover:bg-teal-500 py-2 lg:py-3 font-bold text-white rounded shadow">Uložit</button>
                            <button type="reset" className="flex-1 py-2 lg:py-3 bg-red-600 hover:bg-red-500 font-bold text-white text-center rounded shadow">Storno</button>
                        </div>
                        <Field type="hidden" name="notice" />
                        <Field type="hidden" name="id" />
                        </Form>
                    )}
                </Formik>
                </>
            )}

    </>)
}
