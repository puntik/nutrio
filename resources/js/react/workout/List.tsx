import React, { useEffect, useState } from "react"
import axios from 'axios';
import { Alert } from "../Alert";
import { Item } from "./Item";
import { Add } from "./Add";
import { Workout } from "./Types";
import { Heatmap } from "./Heatmap";
import { Link } from "react-router-dom";

export const List: React.FC = () => {
    const [workouts, setWorkouts] = useState<Workout[]>(
        []
    )

    const [loading, setIsLoading] = useState(
        false
    )

    const [saving, setIsSaving] = useState(
        false
    )

    useEffect(() => {
        const getData = async (): Promise<void> => {
            setIsLoading(true);
            const response = await axios.get('/api/trainings');
            setWorkouts(response.data.data);
        }

        getData().catch((e) => {
            console.log(e)
        })

        setIsLoading(false)
    }, [])

    const addNewRecord = (workout: Workout) => {
        workouts.unshift(workout)
        setWorkouts([...workouts])

        setIsSaving(true)

        const saveData = async (): Promise<void> => {
            const response = await axios.post('api/trainings', workout);
        }

        saveData().catch((e) => {
            console.log(e);
        })
    }

    const to = '/chartjs';

    return (
        <>
            <Add onWorkoutAdded={addNewRecord}/>
            <Heatmap weeksAgo={9}></Heatmap>
            {loading && (
                <Alert text="Loading"></Alert>
            )}

            {workouts.length === 0 && !loading && (
                <Alert text="Empty list"></Alert>
            )}

            <div className="space-y-1 mt-1">
                {workouts.map((training: Workout) => {
                    return (
                        <Item key={training.id} training={training}/>
                    )
                })}
            </div>
            <Link to="/chartjs">New chart</Link>
        </>
    )
}
