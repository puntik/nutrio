import axios from "axios";
import React, { useEffect, useState } from "react"

// @todo: weekday labels

const weekDays: string[] = ['po', 'út', 'st', 'čt', 'pá', 'so', 'ne'];
const months: string[] = ['led', 'úno', 'bře', 'dub', 'kvě', 'črn', 'čvc', 'srp', 'zář', 'říj', 'lis', 'pro'];

const DaySquare: React.FC<{text: string, density?: boolean}> = ({text, density}) => {
    const style = "w-5 h-5 text-sm text-gray-500 rounded ".concat(density ? 'bg-teal-400' : 'bg-teal-50');

    return (
        <div className={style}>{text}</div>
    )
}

const startMonday = (weeksAgo: number): Date => {
    // find first monday n weeks ago
    const today = new Date(); // get the current date
    const mondayEightWeeksAgo = new Date(today.getTime() - (weeksAgo * 7 * 24 * 60 * 60 * 1000)); // calculate the date 8 weeks ago

    // day with index 0
    mondayEightWeeksAgo.setDate(mondayEightWeeksAgo.getDate() - (mondayEightWeeksAgo.getDay() + 6) % 7); // adjust the date to the Monday of that week

    return mondayEightWeeksAgo;
};

const changeMonth = (firstDay: Date): {changed: boolean, month?: number, monthName?: string} => {
    const secondDay = new Date(firstDay.valueOf());
    secondDay.setDate(secondDay.getDate() + 7);
    if (firstDay.getMonth() !== secondDay.getMonth()) {
        return {
            changed: true,
            month: secondDay.getMonth(),
            monthName: months[secondDay.getMonth()],
        };
    }

    return {changed: false};
}

export const Heatmap: React.FC<{weeksAgo?: number}> = ({weeksAgo = 12}) => {

    const [dates, setDates] = useState<string[]>([]);
    const mondayEightWeeksAgo = startMonday(weeksAgo);

    useEffect(() => {
        const fetchData = async () => {
            const dates = (await axios.get('/api/trainings/calendar')).data.map((str: string) => {
                return (new Date(Date.parse(str)).toDateString());
            });
            setDates(dates);
        };

        fetchData().catch((e) => {
            console.log(e);
        })
    }, []);

    return (
        <div className="mt-1 p-1 rounded shadow bg-white flex space-x-1">
        {[...Array(weeksAgo + 1).keys()].map((week) => {
            const xxx = changeMonth(mondayEightWeeksAgo);
            const monthName = xxx.changed? xxx.monthName! : '';

            return (<div key={week} className="space-y-1">
                <DaySquare text={monthName} density={false} />
                {weekDays.map((day) => {
                    // find density for current date
                    const density: boolean = dates.includes(mondayEightWeeksAgo.toDateString());
                    // update date
                    mondayEightWeeksAgo.setDate(mondayEightWeeksAgo.getDate() + 1);
                    // render square
                    return (
                        <DaySquare key={mondayEightWeeksAgo.toDateString()} text="" density={density} />
                    )
                })}
                </div>
            )
        })}
        </div>
    )
}