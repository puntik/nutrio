interface Group {
    id: number,
    title: string,
}

interface Workout {
    id: number,
    notice: string,
    workout_at: string,
    groups: Group[],
    strava_save?: boolean,
    strava_elapsed_time?: string,
    strava_distance?: string,
}

export {
    Group,
    Workout
}