import { Group } from "./Types"

export const groups:Group[] = [
    { id: 1, title: 'Biceps' },
    { id: 8, title: 'Zada' },
    { id: 5, title: 'Prsa' },
    { id: 7, title: 'Triceps' },
    { id: 4, title: 'Nohy' },
    { id: 6, title: 'Ramena' },
    { id: 2, title: 'Bricho' },
    { id: 3, title: 'Kardio' },
]
