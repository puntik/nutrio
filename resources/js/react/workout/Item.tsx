import React from "react"
import { format } from "date-fns";
import { Workout } from "./Types";

interface Props {
    training: Workout
}

export const Item: React.FC<Props> = (props) => {
    const date = Date.parse(props.training.workout_at);

    return (
        <div className="px-1 py-2 bg-white rounded shadow">
            <h2 className="text-gray-700 lg:text-lg">{props.training.notice}</h2>
            <span className="text-gray-700 text-sm lg:text-base">{format(date,'iii Y/MM/dd')}</span>
        </div>
    )
}
