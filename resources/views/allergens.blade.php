@extends('layouts.app')

@section('content')
    <div class="flex flex-wrap">
        <div class="w-full md:w-1/2 lg:w-1/3">
            <allergens-component></allergens-component>
        </div>
    </div>
@endsection

