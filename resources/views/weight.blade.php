@extends('layouts.app', ['noVue' => true])

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script
        src="https://cdn.jsdelivr.net/npm/chartjs-adapter-date-fns/dist/chartjs-adapter-date-fns.bundle.min.js"></script>
@endpush

@section('content')
    <div class="">
        <div class="space-y-4 md:flex md:space-y-0 md:space-x-2">
            <div
                class="md:flex-1 border-0 border-t-4 border-green-200 bg-white bg-gradient-to-br from-green-100 via-white p-4 border rounded shadow">
                <h2 class="text-sm text-gray-500">váha</h2>
                <span
                    class="w-full block py-4 text-center text-gray-600 text-5xl font-bold">{{ $data['weight'] }}kg</span>
                <div class="text-sm text-gray-500">změna <span class="font-bold">{{ $data['diff'] }} kg</span>
                    od {{ $data['prev_weighed_at'] }}
                </div>
            </div>
            <div
                class="md:flex-1 border-0 border-t-4 border-green-200 bg-white bg-gradient-to-br from-green-100 via-white p-4 border rounded shadow">
                <h2 class="text-sm text-gray-500">predikovaná váha</h2>
                <span
                    class="w-full block py-4 text-center text-gray-600 text-5xl font-bold">{{ $data['prediction_weight'] }}kg</span>
                <div class="text-sm text-gray-500">predikce na tři měsíce</div>
            </div>
            <div
                class="md:flex-1 border-0 border-t-4 border-red-200 bg-white bg-gradient-to-br from-red-100 via-white p-4 border rounded shadow">
                <h2 class="text-sm text-gray-500">bmi</h2>
                <span
                    class="w-full block py-4 text-center text-gray-600 text-5xl font-bold">{{ $data['bmi'] }}</span>
                <div class="text-sm text-gray-500">mírná nadváha</div>
            </div>
            <div
                class="md:flex-1 border-0 border-t-4 border-gray-200 bg-white bg-gradient-to-br from-gray-100 via-white p-4 border rounded shadow">
                <h2 class="text-sm text-gray-500">bazálni kalorická spotřeba</h2>
                <span
                    class="w-full block py-4 text-center text-gray-600 text-5xl font-bold">{{ $data['bmr'] }}kcal</span>
                <div class="text-sm text-gray-500"></div>
            </div>
        </div>
    </div>

    <div class="md:flex mt-4">
        <div class="w-full md:w-2/3 bg-white border-t-4 border-teal-200 rounded shadow">
            <table class="table table-auto w-full">
                <thead>
                <tr class="bg-teal-100 text-teal-600 font-thin italic">
                    <th class="py-3">Datum</th>
                    <th class="py-3">Váha</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data['weight_log'] as $w)
                    <tr class="odd:bg-teal-50">
                        <td class="p-2 text-sm">{{ $w->weighed_at->format('Y-m-d') }}</td>
                        <td class="p-2 text-sm">{{ $w->weight }}kg</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="w-full md:w-1/3">
            <form method="post" class="md:p-4">
                @csrf
                <div class="space-y-2">
                    <div class="">
                        <label class="block text-sm" for="weighed_at">Datum</label>
                        <input class="w-full p-2 border text-sm text-gray-800 rounded" id="weighed_at" name="weighed_at"
                               type="date"
                               value="{{ now()->format('Y-m-d') }}"/>
                    </div>
                    <div class="">
                        <label class="block text-sm" for="weight">Nová váha (kg)</label>
                        <input class="w-full p-2 border text-sm text-gray-800 rounded" id="weight" name="weight"
                               type="text"/>
                    </div>
                    <input
                        type="submit"
                        class="w-full shadow bg-teal-500 hover:bg-teal-400 p-3 rounded text-white text-sm"
                        value="Uložit"/>
                </div>
            </form>
        </div>
    </div>
@endsection
