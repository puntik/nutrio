@extends('layouts.app')

@section('content')
    <div class="w-full md:w-1/2">
        <form method="post">
            @csrf
            <div class="space-y-4">
                <div class="text-gray-700">
                    <label for="gender" class="text-sm">Gender</label>

                    <input id="gender" type="text"
                           class="p-2 border w-full text-sm text-gray-800 rounded dark-mode:bg-gray-700 dark-mode:text-white dark-mode:border-gray-600"
                           name="gender"
                           value="{{ old('gender') ?? $user->gender }}">

                </div>

                <div class="text-gray-700">
                    <label for="born_at" class="text-sm">Born at</label>

                    <input id="born_at" type="date"
                           class="p-2 border w-full text-sm text-gray-800 rounded dark-mode:bg-gray-700 dark-mode:text-white dark-mode:border-gray-600"
                           name="born_at"
                           value="{{ old('born_at') ?? $user->born_at?->format('Y-m-d') }}">

                </div>

                <div class="text-gray-700">
                    <label for="height" class="text-sm">Výška</label>

                    <input id="height" type="text"
                           class="p-2 border w-full text-sm text-gray-800 rounded dark-mode:bg-gray-700 dark-mode:text-white dark-mode:border-gray-600"
                           name="height"
                           value="{{ old('height') ?? $user->height }}">

                </div>

                <div class="text-gray-700">
                    <label for="weight" class="text-sm">Váha</label>

                    <input id="weight" type="text"
                           class="p-2 border w-full text-sm text-gray-800 rounded dark-mode:bg-gray-700 dark-mode:text-white dark-mode:border-gray-600"
                           name="weight"
                           value="{{ old('weight') ?? $user->weight }}">

                </div>

                <button type="submit"
                        class="w-full shadow bg-teal-500 hover:bg-teal-400 p-3 rounded text-white text-sm">
                    {{ __('Uložit data') }}
                </button>
            </div>
        </form>
    </div>
@endsection
