<nav class="bg-gray-50 shadow" x-data="{open: false}">
    <div class="max-w-6xl mx-auto px-1">
        <div class="flex justify-between">
            <div class="flex space-x-3">
                <div>
                    <a href="{{ url('/') }}" class="flex w-full items-center py-6 px-1">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-1 text-teal-600"
                             fill="none"
                             viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                  d="M14.828 14.828a4 4 0 01-5.656 0M9 10h.01M15 10h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"/>
                        </svg>
                        <span class="font-bold text-teal-600">{{ config('app.name') }}</span>
                    </a>
                </div>

                <div class="hidden md:flex items-center space-x-1">
                    <a href="{{ url('/bmi') }}" class="py-6 px-1 text-gray-700 hover:text-gray-900">BMI</a>
                    <a href="{{ url('/activities') }}" class="py-6 px-1 text-gray-700 hover:text-gray-900">Aktivity</a>
                    <a href="{{ url('/nutrients') }}" class="py-6 px-1 text-gray-700 hover:text-gray-900">Živiny</a>
                    @if(Auth::check())
                        <a href="{{ url('/weight') }}" class="py-6 px-1 text-gray-700 hover:text-gray-900">Váha</a>
                        <a href="{{ url('/workout') }}" class="py-6 px-1 text-gray-700 hover:text-gray-900">Cvičení</a>
                    @endif
                </div>
            </div>
            <div class="hidden md:flex items-center space-x-1">
                @if(! Auth::check())
                    <a href="{{ url('/login') }}"
                       class="py-2 px-3 text-gray-700 bg-yellow-400 hover:bg-yellow-300 text-yellow-900 hover:text-yellow-800 rounded transition duration-300"
                    >Sign in</a>
                @endif
                @if(Auth::check())
                    <img src="https://i.pravatar.cc/100?img=69" alt="Avatar" class="w-6 h-6 rounded-full"/>
                    <span class="">{{ Auth::user()->email }}</span>
                    <form method="post" action="{{ url('logout') }}">
                        @csrf
                        <button
                            class="ml-2 py-2 px-3 text-gray-700 bg-yellow-400 hover:bg-yellow-300 text-yellow-900 hover:text-yellow-800 rounded">
                            Odhlásit
                        </button>
                    </form>
                @endif
            </div>

            <!--mobile button -->
            <div class="md:hidden flex items-center">
                <button x-on:click="open = ! open">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 border rounded-sm" fill="none"
                         viewBox="0 0 24 24"
                         stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                              d="M4 6h16M4 12h16M4 18h16"/>
                    </svg>
                </button>
            </div>
        </div>

        <!--mobile menu -->
        <div class="md:hidden pl-2 pb-3 space-y-2" x-show="open">
            <a href="{{ url('/') }}" class="block text-gray-700">BMI</a>
            <a href="{{ url('/activities') }}" class="block text-gray-700">Aktivity</a>
            <a href="{{ url('/nutrients') }}" class="block text-gray-700">Živiny</a>
            @if(! Auth::check())
                <a href="{{ url('/login') }}" class="block text-gray-700">Přihlásit</a>
            @endif
            @if(Auth::check())
                <a href="{{ url('/weight') }}" class="block text-gray-700">Váha</a>
                <a href="{{ url('/workout') }}" class="block text-gray-700">Cvičení</a>
                <form method="post" action="{{ url('logout') }}">
                    @csrf
                    <button
                        class="py-2 px-3 text-gray-700 bg-yellow-400 hover:bg-yellow-300 text-yellow-900 hover:text-yellow-800 rounded">
                        Odhlásit
                    </button>
                </form>
            @endif
        </div>
    </div>
</nav>

