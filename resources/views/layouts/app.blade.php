<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet"/>

    @stack('scripts')

    @if(! isset($noVue))
        <script src="{{ asset('js/app_vuejs.js') }}" defer></script>
    @endif

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

</head>
<body class="bg-gray-100">
<div id="app">
    @include('navigation')
    <main class="max-w-6xl mx-auto mt-3 px-1">
        @yield('content')
    </main>
</div>
</body>
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}" defer></script>

</html>
