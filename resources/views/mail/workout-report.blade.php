<x-mail::message>
# Treningovy report za obobi od {{ $start->format('Y-m-d') }}


<x-mail::table>
| Datum         | @foreach($workoutGroups as $id => $group) {{ Str::of($group)->substr(0, 2) }} | @endforeach |
| ------------- | @foreach($workoutGroups as $group) --:| @endforeach --:|
@foreach ($workouts as $date => $groups)
| {{ $date }} | @foreach($workoutGroups as $index => $group) @if(in_array($index, $groups)) x @endif | @endforeach |
@endforeach
</x-mail::table>

<hr />
Thanks,<br>
{{ config('app.name') }}
</x-mail::message>
