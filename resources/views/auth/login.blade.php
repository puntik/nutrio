@extends('layouts.app')

@section('content')
    <form method="POST" action="{{ route('login') }}" role="form">
        <div class="w-full md:w-1/2 space-y-4">
            @csrf
            <div class="">
                <label for="email" class="text-teal-700 font-semibold">Uživatelské jméno</label>

                <input id="email" type="email"
                       class="px-2 py-4 border w-full text-gray-800 rounded dark-mode:bg-gray-700 dark-mode:text-white dark-mode:border-gray-600"
                       name="email"
                       value="{{ old('email') }}" required autocomplete="email" autofocus>

            </div>

            <div class="">
                <label for="password" class="text-teal-700 font-semibold">Heslo</label>

                <input id="password" type="password"
                       class="px-2 py-4 border w-full text-gray-800 rounded dark-mode:bg-gray-700 dark-mode:text-white dark-mode:border-gray-600"
                       name="password"
                       required autocomplete="current-password">
            </div>

            <div class="flex items-center">
                <input class="h-4 w-4 border border-gray-600 rounded shadow" type="checkbox" id="remember"
                       name="remember"/>
                <label class="pl-1 text-teal-700 font-semibold" for="remember">Pamatuj si mě</label>
            </div>

            <div class="space-y-1 text-center">
                <button type="submit"
                        class="w-full shadow bg-teal-500 hover:bg-teal-400 p-4 rounded text-white font-semibold">
                    {{ __('Login') }}
                </button>
                <a href="{{ url('/auth/redirect') }}"
                   class="w-full block shadow bg-blue-500 hover:bg-blue-400 p-4 rounded text-white font-semibold ">
                    Google login
                </a>
            </div>
        </div>
    </form>
    {{--<div class="md:w-1/2">
        <a class="block w-full mt-1 p-3 bg-yellow-400 hover:bg-yellow-500 text-sm text-white text-center rounded shadow"
           href="{{ url('/register') }}">{{ __('Založit nový účet') }}</a>
    </div>
    --}}

    {{--<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
         class="w-8 h-8"
         viewBox="0 0 172 172"
         style=" fill:#000000;">
        <g transform="">
            <g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt"
               stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0"
               font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal">
                <path d="M0,172v-172h172v172z" fill="none"></path>
                <g>
                    <path
                        d="M156.27275,71.96408h-5.77275v-0.29742h-64.5v28.66667h40.50242c-5.90892,16.68758 -21.78667,28.66667 -40.50242,28.66667c-23.74675,0 -43,-19.25325 -43,-43c0,-23.74675 19.25325,-43 43,-43c10.96142,0 20.93383,4.13517 28.52692,10.88975l20.27092,-20.27092c-12.79967,-11.92892 -29.92083,-19.2855 -48.79783,-19.2855c-39.57792,0 -71.66667,32.08875 -71.66667,71.66667c0,39.57792 32.08875,71.66667 71.66667,71.66667c39.57792,0 71.66667,-32.08875 71.66667,-71.66667c0,-4.80525 -0.4945,-9.49583 -1.39392,-14.03592z"
                        fill="#ffc107"></path>
                    <path
                        d="M22.5965,52.64275l23.54608,17.26808c6.37117,-15.77383 21.801,-26.91083 39.85742,-26.91083c10.96142,0 20.93383,4.13517 28.52692,10.88975l20.27092,-20.27092c-12.79967,-11.92892 -29.92083,-19.2855 -48.79783,-19.2855c-27.52717,0 -51.39933,15.54092 -63.4035,38.30942z"
                        fill="#ff3d00"></path>
                    <path
                        d="M86,157.66667c18.5115,0 35.33167,-7.08425 48.04892,-18.60467l-22.18083,-18.7695c-7.19533,5.45025 -16.13933,8.7075 -25.86808,8.7075c-18.6405,0 -34.46808,-11.88592 -40.43075,-28.47317l-23.3705,18.00625c11.86083,23.20925 35.948,39.13358 63.80125,39.13358z"
                        fill="#4caf50"></path>
                    <path
                        d="M156.27275,71.96408h-5.77275v-0.29742h-64.5v28.66667h40.50242c-2.838,8.01592 -7.99442,14.92817 -14.64508,19.96275c0.00358,-0.00358 0.00717,-0.00358 0.01075,-0.00717l22.18083,18.7695c-1.5695,1.42617 23.61775,-17.22508 23.61775,-53.05842c0,-4.80525 -0.4945,-9.49583 -1.39392,-14.03592z"
                        fill="#1976d2"></path>
                </g>
                <path d="" fill="none"></path>
            </g>
        </g>
    </svg>--}}

@endsection
