@extends('layouts.app')

@section('content')
    <div class="w-full md:w-1/2">
        <form method="POST" action="{{ route('register') }}">
            @csrf
            <div class="space-y-4">
                <div class="text-gray-700">
                    <label class="text-sm">Uživatelské jméno (e-mail)</label>

                    <input id="email" type="email"
                           class="p-2 border w-full text-sm text-gray-800 rounded dark-mode:bg-gray-700 dark-mode:text-white dark-mode:border-gray-600"
                           name="email"
                           value="{{ old('email') }}" required autocomplete="email" autofocus>

                </div>

                @error('email')
                <span class="invalid-feedback" role="alert">Email
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror

                <div class="text-gray-700">
                    <label class="text-sm">Jméno</label>

                    <input id="name" type="text"
                           class="p-2 border w-full text-sm text-gray-800 rounded dark-mode:bg-gray-700 dark-mode:text-white dark-mode:border-gray-600"
                           name="name"
                           value="{{ old('name') }}">

                </div>

                @error('name')
                <span class="invalid-feedback" role="alert">Name
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror

                <div class="text-gray-700">
                    <label class="text-sm">Heslo</label>

                    <input id="password" type="password"
                           class="p-2 border w-full text-sm text-gray-800 rounded dark-mode:bg-gray-700 dark-mode:text-white dark-mode:border-gray-600"
                           name="password"
                           required autocomplete="new-password">
                </div>

                @error('password')
                <span class="invalid-feedback" role="alert">Pass:
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror

                <div class="text-gray-700">
                    <label class="text-sm">Ověřit heslo</label>

                    <input id="password-confirm" type="password"
                           class="p-2 border w-full text-sm text-gray-800 rounded dark-mode:bg-gray-700 dark-mode:text-white dark-mode:border-gray-600"
                           name="password_confirmation"
                           required autocomplete="new-password">
                </div>

                <button type="submit"
                        class="w-full shadow bg-teal-500 hover:bg-teal-400 p-3 rounded text-white text-sm">
                    {{ __('Register') }}
                </button>
            </div>
        </form>
    </div>

@endsection
