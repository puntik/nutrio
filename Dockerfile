FROM composer:latest AS composer

FROM php:8.1.16

COPY --from=composer /usr/bin/composer /usr/local/bin/composer

# Install dependencies
RUN apt-get update && apt-get install -y zip libzip-dev unzip curl build-essential autoconf libcurl4-openssl-dev git # libicu-dev

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install extensions
RUN docker-php-ext-install zip  # pdo_mysql zip exif pcntl
# RUN pecl install xdebug
# RUN docker-php-ext-enable xdebug

# Add user for laravel application
RUN groupadd -g 1000 www
RUN useradd -u 1000 -ms /bin/bash -g www www

WORKDIR /var/www

RUN echo "memory_limit = 128M" > /usr/local/etc/php/conf.d/memory-limit.ini
USER www

# kopirovani nepotrebuju, pripojuju jako volume
# COPY --chown=www:www src/ /var/www

EXPOSE 8000

EXPOSE 9003

# spustit server
CMD ["php", "artisan", "serve", "--host", "0.0.0.0"]
