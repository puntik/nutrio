# Nutrio

## Linky

- [nutrio2.rem.cz](http://nutrio2.rem.cz) (test a/nebo preprod)
- nutrio.rem.cz (dočasně produkční)

## Cíle

## Fáze


## Development

Pouzivame docker-sync - par kroku k rozchozeni ... 

~~~
# install docker-sync locally (for logged user)
gem install --user-install docker-sync

# or globally via
sudo gem install docker-sync

# run in command line  
docker-sync stop && docker-sync start

# start your docker stack
docker-compose -f docker-compose.yml -f docker-compose-dev.yml up -d
~~~

Development links:

- [app.nutrio.loc](http://app.nutrio.loc) - runs without xdebug
- [kibana](http://app.nutrio.loc:5601/app/kibana#/home) - analytic tool on top of elastic search

