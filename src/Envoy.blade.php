@task('clean')
    if test -L {{ $appPath }}/release; then
        echo "Removing {{ $appPath }}/release symlink"
        rm {{ $appPath }}/release
    fi

    echo "Removing old release directories"
    cd {{ $appPath }}/releases
    ls -1t | tail -n +8 | xargs rm -rf
@endtask

@task('init')
    if test -d $appPath; then
        echo "Target directory {{ $appPath }} already exists"
    else
        echo "Creating project directories"
        mkdir -p {{ $appPath }}/releases
        mkdir -p {{ $appPath }}/shared
        mkdir -p {{ $appPath }}/shared/storage
        mkdir -p {{ $appPath }}/shared/database

        echo "Creating important files"
        touch {{ $appPath }}/shared/.env
        touch {{ $appPath }}/shared/database/database.sqlite

        echo "Now you should edit your .env file to prepare deploying your laravel project"
    fi
@endtask

@task('artisan:migrate')
    cd {{ $releasePath }}
    php artisan migrate --force
@endtask

@task('artisan:config:cache')
    cd {{ $releasePath }}
    php artisan config:clear
    php artisan config:cache
@endtask

@task('artisan:cache:clear')
    cd {{ $releasePath }}
    php artisan cache:clear
@endtask

@task('artisan:route:cache')
    cd {{ $releasePath }}
    php artisan route:cache
@endtask

@task('fpm:reload')
    {{-- gentoo version --}}
    {{-- sudo -S rc-service php-fpm reload --}}
    sudo /etc/init.d/php-fpm reload
@endtask