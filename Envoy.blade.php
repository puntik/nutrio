@servers(['oak'=> 'puntik@oak.rtx.cz'])

@setup
    $appPath = "/home/puntik/www/nutrio2.rem.cz";
    $git = 'git@bitbucket.org:puntik/nutrio.git';

    $now = now()->format('Ymd\THis');
    $currentPath = sprintf('%s/current', $appPath);
    $releasePath = sprintf('%s/release', $appPath);
@endsetup

@import('src/Envoy.blade.php')

@task('update-code')
    if test -d {{ $releasePath }}; then
        echo "There is not empty release directory. I cannot update code."
    else
        cd {{ $appPath }}/releases

        git clone {{ $git }} {{ $now }}

        cd {{ $now }}

        composer install --no-dev --no-interaction --quiet
        # npm --silent install
        # npm run build

        ln -s {{ $appPath }}/releases/{{ $now }} {{ $releasePath }}
    fi
@endtask

@task('finish')
    cd {{ $appPath }}
    {{-- vyrobit current link --}}
    echo {{ $currentPath }}
    rm {{ $currentPath }}
    ln -s releases/{{ $now }} {{ $currentPath }}
    {{-- smazat release link --}}
    rm {{ $releasePath }}
@endtask

@task('shared')
    echo "Creating links for shared files"
    ln -s {{ $appPath }}/shared/.env {{ $releasePath }}/.env
    ln -s {{ $appPath }}/shared/database/database.sqlite {{ $releasePath }}/database/database.sqlite

    echo "Creating links for storage directory"
    cp -R {{ $releasePath }}/storage/* {{ $appPath }}/shared/storage/
    rm -rf {{ $releasePath }}/storage
    ln -s {{ $appPath }}/shared/storage {{ $releasePath }}/storage
@endtask

@story('deploy')
    update-code
    shared
    artisan:migrate
    artisan:config:cache
    artisan:cache:clear
    artisan:route:cache
    finish
    fpm:reload
@endstory

{{--
Postup instalace: 
- envoy run init
- updatovat .env v shared
- envoy run deploy
--}}

{{--
Postup updatu/releasu
- envoy run deploy
--}}