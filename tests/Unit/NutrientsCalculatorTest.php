<?php

namespace Unit;

use App\Services\NutrientsCalculator;
use PHPUnit\Framework\TestCase;

class NutrientsCalculatorTest extends TestCase
{

    /** @var NutrientsCalculator */
    private $nutrientsCalculator;

    protected function setUp(): void
    {
        $this->nutrientsCalculator = new NutrientsCalculator();

        parent::setUp();
    }

    /**
     * @test
     */
    public function nutrients_calculator_works(): void
    {
        // Given
        $targetEnergy = 2500;

        // When
        $data = $this->nutrientsCalculator->calculate($targetEnergy);

        // Then
        $this->assertIsArray($data);
    }
}
