<?php

namespace Tests\Unit;

use Egulias\EmailValidator\EmailValidator;
use Egulias\EmailValidator\Validation\RFCValidation;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class ExampleTest extends TestCase
{

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $data = [
            'v@gmail.com'                => '*@gmail.com',
            'vk@gmail.com'               => '**@gmail.com',
            'vkl@gmail.com'              => 'v*l@gmail.com',
            'vklik1234@gmail.com'        => 'v*******4@gmail.com',
            'vlastimil.klik@gmail.com'   => 'v************k@gmail.com',
            'dan.vavra+google@gmail.com' => 'd**************e@gmail.com',
        ];

        foreach ($data as $email => $expected) {
            $output = $this->str_hide_email($email);

            // dd($output, $expected);
            self::assertSame($expected, $output);
        }
    }

    private function str_hide_email(
        string $email,
        string $fill = '*'
    ): string {
        // try to find at sign
        $atPosition = strpos($email, '@');

        // it does not contain at sign
        if (! $atPosition) {
            return $email;
        }

        [$start, $multiplier] = $atPosition <= 2 ? [0, $atPosition] : [1, $atPosition - 2];

        return substr_replace(
            $email,
            str_repeat($fill, $multiplier),
            $start,
            $multiplier);
    }
}
